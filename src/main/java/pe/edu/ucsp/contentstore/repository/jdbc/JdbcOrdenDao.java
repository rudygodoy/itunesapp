package pe.edu.ucsp.contentstore.repository.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import pe.edu.ucsp.contentstore.domain.Contenido;

import pe.edu.ucsp.contentstore.domain.Orden;
import pe.edu.ucsp.contentstore.repository.OrdenDao;

@Repository
public class JdbcOrdenDao extends JdbcGenericDao<Orden, Long> implements OrdenDao {

	private final OrdenMapper mapper = new OrdenMapper();

	@Override
	public void update(final Orden orden) {
		String sql = "UPDATE " + getTableName() + " SET Id = ?";
		jdbcTemplate.update(sql, orden.getId());
	}
        @Override
	protected List<Orden> findById(Long id_usuario) {
		String sql = "SELECT * FROM" + getTableName() + " t join contenidos c on(t.id_contenido = c.id) WHERE t.id_usuario = ? LIMIT 10";
		return jdbcTemplate.query(sql, getRowMapper(), id_usuario);
	}
	@Override
	protected SimpleJdbcInsert createJdbcInsert() {
		return new SimpleJdbcInsert(jdbcTemplate.getDataSource())
		.withTableName(getTableName()).usingGeneratedKeyColumns("id");
	}

	@Override
	protected RowMapper<Orden> getRowMapper() {
		return mapper;
	}

	@Override
	protected String getTableName() {
		return Orden.TABLE_NAME;
	}
        
	public static class OrdenMapper implements RowMapper<Orden>{
		public Orden mapRow(ResultSet rs, int rowNum) throws SQLException{
			Orden orden = new Orden();
			orden.setId(rs.getLong("id"));
			orden.setCodigo(rs.getString("codigo"));
			orden.setFecha(rs.getDate("fecha"));
			return orden;
		}
	}

}
