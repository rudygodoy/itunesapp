package pe.edu.ucsp.contentstore.repository.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.contentstore.domain.Categoria;
import pe.edu.ucsp.contentstore.repository.CategoriaDao;

@Repository
public class JdbcCategoriaDao extends JdbcGenericDao<Categoria, Long> implements CategoriaDao {

	private final CategoriaMapper mapper = new CategoriaMapper();

	@Override
	public void update(final Categoria categoria) {
		String sql = "UPDATE " + getTableName() + " SET codigo = ?, nombre = ?, id_categoria = ? WHERE id = ?";
		jdbcTemplate.update(sql, categoria.getCodigo(), categoria.getNombre(), categoria.getId_categoria(), categoria.getId());
	}

		
	@Override
	protected SimpleJdbcInsert createJdbcInsert() {
		return new SimpleJdbcInsert(jdbcTemplate.getDataSource())
		.withTableName(getTableName()).usingGeneratedKeyColumns("id");
	}

	@Override
	protected RowMapper<Categoria> getRowMapper() {
		return mapper;
	}

	@Override
	protected String getTableName() {
		return Categoria.TABLE_NAME;
	}

	public static class CategoriaMapper implements RowMapper<Categoria>{
		public Categoria mapRow(ResultSet rs, int rowNum) throws SQLException{
			Categoria categoria = new Categoria();
			categoria.setId(rs.getLong("id"));
			categoria.setCodigo(rs.getString("codigo"));
			categoria.setNombre(rs.getString("nombre"));
			categoria.setDescripcion(rs.getString("descripcion"));
			categoria.setId_categoria(rs.getLong("id_categoria"));
			return categoria;
		}
	}
}
