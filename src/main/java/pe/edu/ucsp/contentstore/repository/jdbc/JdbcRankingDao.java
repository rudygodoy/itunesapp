package pe.edu.ucsp.contentstore.repository.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameterValue;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.contentstore.domain.Ranking;
import pe.edu.ucsp.contentstore.repository.RankingDao;

@Repository
public class JdbcRankingDao extends JdbcGenericDao<Ranking, Long> implements RankingDao {

	private final RankingMapper mapper = new RankingMapper();

	@Override
	public void update(final Ranking ranking) {
		String sql = "UPDATE " + getTableName() + " SET valor = ?, id_usuario = ?, id_contenido = ? WHERE id = ?";
		jdbcTemplate.update(sql, ranking.getValor(), ranking.getId_usuario(), ranking.getId_contenido(), ranking.getId());
	}

        @Override
	protected List<Ranking> findByIdUsuario(long id_usuario) {
                String sql = "SELECT * FROM " + getTableName() + " WHERE id_usuario = ?";
		return jdbcTemplate.query(sql, getRowMapper(), id_usuario);
	}
        
	@Override
	protected SimpleJdbcInsert createJdbcInsert() {
		return new SimpleJdbcInsert(jdbcTemplate.getDataSource())
		.withTableName(getTableName()).usingGeneratedKeyColumns("id");
	}

	@Override
	protected RowMapper<Ranking> getRowMapper() {
		return mapper;
	}
	
	@Override
	public void save(Ranking ranking){
		System.out.println(ranking.toString());
		super.save(ranking);
	}


	@Override
	protected String getTableName() {
		return Ranking.TABLE_NAME;
	}

	public static class RankingMapper implements RowMapper<Ranking>{
		public Ranking mapRow(ResultSet rs, int rowNum) throws SQLException{
			Ranking ranking = new Ranking();
			ranking.setId(rs.getLong("id"));
			ranking.setValor(rs.getInt("valor"));
			ranking.setId_contenido(rs.getLong("id_contenido"));
			ranking.setId_usuario(rs.getLong("id_usuario"));
			return ranking;
		}
	}
}
