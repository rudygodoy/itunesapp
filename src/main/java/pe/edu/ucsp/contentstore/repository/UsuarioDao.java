package pe.edu.ucsp.contentstore.repository;

import java.util.List;

import pe.edu.ucsp.contentstore.domain.Usuario;

public interface UsuarioDao extends GenericDao<Usuario, Long> {

	List<Usuario> findByEmail(String email);
	List<Usuario> filterByEmail(String email);
	
}
