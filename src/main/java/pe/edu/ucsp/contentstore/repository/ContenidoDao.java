package pe.edu.ucsp.contentstore.repository;

import java.util.List;

import pe.edu.ucsp.contentstore.domain.Contenido;
import pe.edu.ucsp.contentstore.domain.Usuario;

public interface ContenidoDao extends GenericDao<Contenido, Long> {

// TODO
	
	List<Contenido> findByCategory(Long categoria_id);


}
