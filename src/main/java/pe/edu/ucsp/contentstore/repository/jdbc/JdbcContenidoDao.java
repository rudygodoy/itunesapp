package pe.edu.ucsp.contentstore.repository.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.contentstore.domain.Contenido;
import pe.edu.ucsp.contentstore.repository.ContenidoDao;

@Repository
public class JdbcContenidoDao extends JdbcGenericDao<Contenido, Long> implements ContenidoDao {

	private final ContenidoMapper mapper = new ContenidoMapper();

	@Override
	public void update(final Contenido contenido) {
		String sql = "UPDATE " + getTableName() + " SET codigo = ?, autor = ?, descripcion = ?, precio = ?, tipo = ?, tamano = ?, id_categoria = ?, activo = ? WHERE id = ?";
		jdbcTemplate.update(sql, contenido.getCodigo(), contenido.getAutor(), contenido.getDescripcion(), contenido.getPrecio(), contenido.getTipo(), contenido.getTamano(), contenido.getId_categoria(), contenido.getActivo(), contenido.getId());
	}

	@Override
	public List<Contenido> findByCategory(Long categoria_id) {
		String sql = "SELECT * FROM " + getTableName() + " WHERE id_categoria = ?";
		return jdbcTemplate.query(sql, getRowMapper(), categoria_id);
	}

	@Override
	public void save(Contenido contenido){
		super.save(contenido);
	}
		
	@Override
	protected SimpleJdbcInsert createJdbcInsert() {
		return new SimpleJdbcInsert(jdbcTemplate.getDataSource())
		.withTableName(getTableName()).usingGeneratedKeyColumns("id");
	}

	@Override
	protected RowMapper<Contenido> getRowMapper() {
		return mapper;
	}

	@Override
	protected String getTableName() {
		return Contenido.TABLE_NAME;
	}

	public static class ContenidoMapper implements RowMapper<Contenido>{
		public Contenido mapRow(ResultSet rs, int rowNum) throws SQLException{
			Contenido contenido = new Contenido();
			contenido.setId(rs.getLong("id"));
			contenido.setCodigo(rs.getString("codigo"));
			contenido.setAutor(rs.getString("autor"));
			contenido.setDescripcion(rs.getString("descripcion"));
			contenido.setPrecio(rs.getDouble("precio"));
			contenido.setTipo(rs.getString("tipo"));
			contenido.setTamano(rs.getDouble("tamano"));
			contenido.setPeso(rs.getDouble("peso"));
			contenido.setId_categoria(rs.getLong("id_categoria"));
			contenido.setData(rs.getBytes("data"));
			contenido.setActivo(rs.getBoolean("activo"));
			return contenido;
		}
	}
}
