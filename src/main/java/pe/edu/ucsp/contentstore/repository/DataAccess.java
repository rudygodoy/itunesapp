package pe.edu.ucsp.contentstore.repository;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public interface DataAccess {

	public Connection connect(String hostname, String port, String database, String username, String password);
}
