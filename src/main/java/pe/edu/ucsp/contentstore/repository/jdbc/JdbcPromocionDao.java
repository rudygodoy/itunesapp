package pe.edu.ucsp.contentstore.repository.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.contentstore.domain.Promocion;
import pe.edu.ucsp.contentstore.repository.PromocionDao;

@Repository
public class JdbcPromocionDao extends JdbcGenericDao<Promocion, Long> implements PromocionDao {

	private final PromocionMapper mapper = new PromocionMapper();

	@Override
	public void update(final Promocion promocion) {
		String sql = "UPDATE " + getTableName() + " SET fecha_inicio = ?, fecha_fin = ?, descuento = ?";
		jdbcTemplate.update(sql, promocion.getFechaInicio(), promocion.getFechaFin(), promocion.getDescuento());
	}

	
	@Override
	protected SimpleJdbcInsert createJdbcInsert() {
		return new SimpleJdbcInsert(jdbcTemplate.getDataSource())
		.withTableName(getTableName()).usingGeneratedKeyColumns("id");
	}

	@Override
	protected RowMapper<Promocion> getRowMapper() {
		return mapper;
	}

	@Override
	protected String getTableName() {
		return Promocion.TABLE_NAME;
	}

	public static class PromocionMapper implements RowMapper<Promocion>{
		public Promocion mapRow(ResultSet rs, int rowNum) throws SQLException{
			Promocion promocion = new Promocion();
			promocion.setId(rs.getLong("id"));
			promocion.setFechaInicio(rs.getDate("fecha_inicio"));
			promocion.setFechaFin(rs.getDate("fecha_fin"));
			promocion.setDescuento(rs.getDouble("descuento"));
			return promocion;
		}
	}
}
