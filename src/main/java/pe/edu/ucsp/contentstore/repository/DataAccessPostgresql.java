package pe.edu.ucsp.contentstore.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataAccessPostgresql implements DataAccess {

	@Override
	public Connection connect(String hostname, String port, String database,
			String username, String password) {
		{
			try{
				Class.forName("org.postgresql.Driver");
			} catch(ClassNotFoundException e){
				System.out.println("Missing driver");
				e.printStackTrace();
				return null;
			}

			Connection connection = null;

			try {
				String connectionString = "jdbc:postgresql://"+ hostname +":"+ port +"/"+ database +", "+ username +","+ password;
				connection = DriverManager.getConnection(connectionString);

			} catch (SQLException e) {
				System.out.println("No logro conectarse a la base de datos.");
				e.printStackTrace();
				return null;
			}

			return connection;
		}		
	}
}
