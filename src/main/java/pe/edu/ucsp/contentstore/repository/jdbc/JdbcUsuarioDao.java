package pe.edu.ucsp.contentstore.repository.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameterValue;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Repository;

import pe.edu.ucsp.contentstore.domain.Usuario;
import pe.edu.ucsp.contentstore.repository.UsuarioDao;


@Repository
public class JdbcUsuarioDao extends JdbcGenericDao<Usuario, Long> implements UsuarioDao {

	private final UsuarioMapper mapper = new UsuarioMapper();
	protected PasswordEncoder encoder = new Md5PasswordEncoder();

	@Override
	public void update(final Usuario usuario) {
		String sql = "UPDATE " + getTableName() + " SET nombre = ?, apellido = ?, email = ?, codigo = ?, tipo = ? WHERE id = ?";
		jdbcTemplate.update(sql, usuario.getNombre(), usuario.getApellido(), usuario.getEmail(), usuario.getCodigo(), usuario.getTipo(), usuario.getId());
	}

	@Override
	public void save(Usuario usuario){
		usuario.setClave(encoder.encodePassword(usuario.getClave(), null));
		super.save(usuario);
	}
	
	@Override
	public List<Usuario> findByEmail(String userEmail) {
		String sql = "SELECT * FROM " + getTableName() + " WHERE email = ? AND activo = true";
//		SqlParameterSource namedParameters = new MapSqlParameterSource("user_email", userEmail);
		return jdbcTemplate.query(sql, getRowMapper(), userEmail);
	}

	@Override
	public List<Usuario> filterByEmail(String email) {
    String sql = "SELECT * FROM " + getTableName() + " WHERE email LIKE :email";
    SqlParameterSource namedParameters = new MapSqlParameterSource("email", email);
    return jdbcTemplate.query(sql, getRowMapper(), namedParameters);
	}
	
	@Override
	protected SimpleJdbcInsert createJdbcInsert() {
		return new SimpleJdbcInsert(jdbcTemplate.getDataSource())
		.withTableName(getTableName()).usingGeneratedKeyColumns("id");
	}

	@Override
	protected RowMapper<Usuario> getRowMapper() {
		return mapper;
	}

	@Override
	protected String getTableName() {
		return Usuario.TABLE_NAME;
	}

	public static class UsuarioMapper implements RowMapper<Usuario>{
		public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException{
			Usuario usuario = new Usuario();
			usuario.setId(rs.getLong("id"));
			usuario.setNombre(rs.getString("nombre"));
			usuario.setApellido(rs.getString("apellido"));
			usuario.setCodigo(rs.getString("codigo"));
			usuario.setClave(rs.getString("clave"));
			usuario.setEmail(rs.getString("email"));
			usuario.setCredito(rs.getDouble("credito"));
			usuario.setTipo(rs.getString("tipo"));
			usuario.setActivo(rs.getBoolean("activo"));
			return usuario;
		}
	}
}
