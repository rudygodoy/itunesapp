package pe.edu.ucsp.contentstore.controller;

import javax.inject.Inject;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import pe.edu.ucsp.contentstore.domain.Usuario;
import pe.edu.ucsp.contentstore.repository.UsuarioDao;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

		Logger logger = Logger.getLogger(getClass());

		@Inject
		UsuarioDao usuarioDao;

		@Inject
		Validator validator;

		@RequestMapping("/list.html")
		public ModelAndView list() {
			return new ModelAndView("usuario/list", "usuarios", usuarioDao.findAll());
		}

		@RequestMapping("/{id}/details.html")
		public ModelAndView details(@PathVariable Long id) {
			ModelAndView view = new ModelAndView();
			view.addObject("usuario", usuarioDao.find(id));
			view.setViewName("usuario/details");
			return view;
		}

		@RequestMapping("/{id}/edit.html")
		public ModelAndView edit(@PathVariable Long id) {
			ModelAndView view = new ModelAndView();
			view.addObject("usuario", usuarioDao.find(id));
			view.setViewName("usuario/edit");
			return view;
		}

		@RequestMapping("/add.html")
		public ModelAndView add() {
			ModelAndView view = new ModelAndView();
			view.addObject("usuario", new Usuario());
			view.setViewName("usuario/edit");
			return view;
		}

		@RequestMapping(value = "/save.html", method = RequestMethod.POST)
		public ModelAndView save(@ModelAttribute("usuario") @Valid Usuario user, BindingResult result, SessionStatus status) {
			logger.warn("Errores encontrados en total: " + result.getErrorCount());
			if (user.getId() == null) {
				usuarioDao.save(user);
				status.setComplete();
			}
			else {
				usuarioDao.update(user);
				status.setComplete();
			}
			return new ModelAndView(result.getErrorCount() > 0 ? "usuario/edit" : "usuario/list");
//			return new ModelAndView("user/save");
		}
}
