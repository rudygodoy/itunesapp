package pe.edu.ucsp.contentstore.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pe.edu.ucsp.contentstore.domain.Usuario;
import pe.edu.ucsp.contentstore.repository.UsuarioDao;
import pe.edu.ucsp.contentstore.repository.CategoriaDao;

@Controller
public class SessionController {
	
	protected PasswordEncoder encoder = new Md5PasswordEncoder();
	@Autowired
    private ServletContext servletContext;
	
	@Inject
	UsuarioDao usuarioDao;

	@Inject
	CategoriaDao categoriaDao;
		
	@RequestMapping("/login.html")
	public ModelAndView showLogin(){
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	public void login(String username, String password, HttpServletRequest request, HttpServletResponse response) throws IOException{
		List<Usuario> usuarios;
		Usuario current_user;
		usuarios = usuarioDao.findByEmail(username);
		if (usuarios.size() > 0){
			current_user = usuarios.get(0);
			if(current_user.getClave().equalsIgnoreCase(encoder.encodePassword(password, null))){
				request.getSession().setAttribute("username", current_user.getEmail());
				request.getSession().setAttribute("usuario_id", current_user.getId());
				response.sendRedirect( servletContext.getContextPath() + "/home.html");
			} else {
				response.sendRedirect( servletContext.getContextPath() + "/login.html");
			}
		} else {
			response.sendRedirect( servletContext.getContextPath() + "/login.html");
		}
	}
	
	@RequestMapping("/logout.html")
	public void logout(HttpServletResponse response, HttpServletRequest request) throws IOException {
		request.getSession().setAttribute("username", null);
	}

	@RequestMapping("/home.html")
	public ModelAndView home(HttpServletResponse response, HttpServletRequest request) throws IOException {
		ModelAndView view = new ModelAndView();
		view.addObject("current_user", request.getSession().getAttribute("username"));
		view.addObject("categorias", categoriaDao.findAll());
		view.setViewName("home");
		return view;
	}
}
