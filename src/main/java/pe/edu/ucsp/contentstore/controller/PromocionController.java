package pe.edu.ucsp.contentstore.controller;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import pe.edu.ucsp.contentstore.domain.Promocion;
import pe.edu.ucsp.contentstore.repository.PromocionDao;

@Controller
@RequestMapping("/promocion")
public class PromocionController {

		Logger logger = Logger.getLogger(getClass());
		
		@Autowired
	    private ServletContext servletContext;

		@Inject
		PromocionDao promocionDao;


		
		@Inject
		Validator validator;

		@RequestMapping("/list.html")
		public ModelAndView list() {
			return new ModelAndView("promocion/list", "promociones",promocionDao.findAll());
		}

		@RequestMapping("/{id}/details.html")
		public ModelAndView details(@PathVariable Long id) {
			ModelAndView view = new ModelAndView();
			view.addObject("promocion", promocionDao.find(id));
			view.setViewName("promocion/details");
			return view;
		}

		@RequestMapping("/{id}/edit.html")
		public ModelAndView edit(@PathVariable Long id) {
			ModelAndView view = new ModelAndView();
			view.addObject("promocion", promocionDao.find(id));
			view.setViewName("promocion/edit");
			return view;
		}

		@RequestMapping("/add.html")
		public ModelAndView add() {
			ModelAndView view = new ModelAndView();
			view.addObject("promocion", new Promocion());
			view.setViewName("promocion/edit");
			return view;
		}

		@RequestMapping(value = "/save.html", method = RequestMethod.POST)
		public ModelAndView save(@ModelAttribute("promocion") @Valid Promocion promocion, BindingResult result, SessionStatus status, HttpServletResponse response) {
			logger.warn("Errores encontrados en total: " + result.getErrorCount());
			if (promocion.getId() == null) {
				promocionDao.save(promocion);
				status.setComplete();
				try {
					response.sendRedirect( servletContext.getContextPath() + "/promocion/list.html");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				promocionDao.update(promocion);
				status.setComplete();
			}
			return new ModelAndView(result.getErrorCount() > 0 ? "promocion/edit" : "promocion/list");
//			return new ModelAndView("user/save");
		}
}
