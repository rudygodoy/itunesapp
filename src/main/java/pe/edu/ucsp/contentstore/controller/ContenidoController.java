package pe.edu.ucsp.contentstore.controller;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import pe.edu.ucsp.contentstore.domain.Contenido;
import pe.edu.ucsp.contentstore.repository.ContenidoDao;
import pe.edu.ucsp.contentstore.repository.CategoriaDao;
import pe.edu.ucsp.contentstore.repository.UsuarioDao;

@Controller
@RequestMapping("/contenido")
public class ContenidoController {

		Logger logger = Logger.getLogger(getClass());

		@Autowired
	    private ServletContext servletContext;

		@Inject
		ContenidoDao contenidoDao;

		@Inject
		CategoriaDao categoriaDao;

		@Inject 
		UsuarioDao usuarioDao;
		
		@Inject
		Validator validator;

		@RequestMapping("/list.html")
		public ModelAndView list() {
			return new ModelAndView("contenido/list", "contenidos", contenidoDao.findAll());
		}

		@RequestMapping("/{id}/details.html")
		public ModelAndView details(@PathVariable Long id, HttpServletRequest request) {
			ModelAndView view = new ModelAndView();
			view.addObject("contenido", contenidoDao.find(id));
			view.setViewName("contenido/details");
			return view;
		}

		@RequestMapping("/{id}/edit.html")
		public ModelAndView edit(@PathVariable Long id) {
			ModelAndView view = new ModelAndView();
			view.addObject("categorias", categoriaDao.findAll());
			view.addObject("contenido", contenidoDao.find(id));
			view.setViewName("contenido/edit");
			return view;
		}

		@RequestMapping("/add.html")
		public ModelAndView add() {
			ModelAndView view = new ModelAndView();
			view.addObject("categorias", categoriaDao.findAll());
			view.addObject("contenido", new Contenido());
			view.setViewName("contenido/edit");
			return view;
		}

		@RequestMapping(value = "/save.html", method = RequestMethod.POST)
		public ModelAndView save(@ModelAttribute("contenido") @Valid Contenido content, BindingResult result, SessionStatus status, HttpServletResponse response) {
			logger.warn("Errores encontrados en total: " + result.getErrorCount());
			System.out.println("contenido id: " + content.getId());
			if (content.getId() == null) {
				contenidoDao.save(content);
				status.setComplete();
				try {
					response.sendRedirect( servletContext.getContextPath() + "/contenido/list.html");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				contenidoDao.update(content);
				status.setComplete();
				try {
					response.sendRedirect( servletContext.getContextPath() + "/contenido/list.html");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return new ModelAndView(result.getErrorCount() > 0 ? "contenido/edit" : "contenido/list");
//			return new ModelAndView("user/save");
		}
}
