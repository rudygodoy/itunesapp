package pe.edu.ucsp.contentstore.controller;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import pe.edu.ucsp.contentstore.domain.Ranking;
import pe.edu.ucsp.contentstore.repository.RankingDao;

@Controller
@RequestMapping("/ranking")
public class RankingController {

		Logger logger = Logger.getLogger(getClass());
		
		@Autowired
	    private ServletContext servletContext;

		@Inject
		RankingDao rankingDao;
		
		@Inject
		Validator validator;

	@RequestMapping(value = "/save.html", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("ranking") @Valid Ranking ranking,
			BindingResult result, SessionStatus status,
			HttpServletResponse response) {
		logger.warn("Errores encontrados en total: " + result.getErrorCount());
			rankingDao.save(ranking);
			status.setComplete();
		try {
			response.sendRedirect(servletContext.getContextPath()
					+ "/contenido/list.html");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ModelAndView(result.getErrorCount() > 0 ? "contenido/edit"
				: "contenido/list");
	}
}
