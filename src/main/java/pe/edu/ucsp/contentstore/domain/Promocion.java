package pe.edu.ucsp.contentstore.domain;

import java.util.Date;

public class Promocion implements BaseEntity<Long>{
	
	public static String TABLE_NAME = "promociones";

	private Long id;
	private Date fecha_inicio;
	private Date fecha_fin;
	private Double descuento;
		

	@Override
	public Long getId() {
		return id;
	}


	@Override
	public void setId(Long id) {
		this.id = id;		
	}
	
	public Date getFechaInicio(){
		return fecha_inicio;
	}
	
	public void setFechaInicio(Date fecha_inicio){
		this.fecha_inicio = fecha_inicio;
	}
	
	public Date getFechaFin(){
		return fecha_fin;
	}

	public void setFechaFin(Date fecha_fin){
		this.fecha_fin = fecha_fin;
	}
	
	public Double getDescuento(){
		return descuento;
	}
	
	public void setDescuento(Double descuento){
		this.descuento = descuento;
	}
}
