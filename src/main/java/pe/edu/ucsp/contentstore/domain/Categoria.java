package pe.edu.ucsp.contentstore.domain;

public class Categoria implements BaseEntity<Long> {

	public final static String TABLE_NAME = "categorias";
	
	private Long id;
	private String codigo;
	private String nombre;
	private String descripcion;
	private Long id_categoria;
	
	public Categoria() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;		
	}

	public String getCodigo(){
		return codigo;
	}
	
	public void setCodigo(String codigo){
		this.codigo = codigo;
	}

	public String getNombre(){
		return nombre;
	}
	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	
	public String getDescripcion(){
		return descripcion;
	}
	
	public void setId_categoria(Long pid){
		this.id_categoria = pid;
	}
	public Long getId_categoria(){
		return id_categoria;	
	}
	
}
