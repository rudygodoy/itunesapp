package pe.edu.ucsp.contentstore.domain;

public class Ranking implements BaseEntity<Long> {

	public static String TABLE_NAME = "ranking";

	private Long id;
	private Long id_contenido;
	private Long id_usuario;
	private Integer valor;


	public Ranking(){

	}


	@Override
	public Long getId() {
		return id;
	}


	@Override
	public void setId(Long id) {
		this.id = id;		
	}


	public Integer getValor() {
		return valor;
	}


	public void setValor(Integer valor) {
		this.valor = valor;
	}


	public Long getId_contenido() {
		return id_contenido;
	}


	public void setId_contenido(Long id_contenido) {
		this.id_contenido = id_contenido;
	}


	public Long getId_usuario() {
		return id_usuario;
	}


	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}
}