package pe.edu.ucsp.contentstore.domain;

public class Contenido implements BaseEntity<Long> {
	
	public static String TABLE_NAME = "contenidos";
	
	private Long id;
	private Double peso;
	private String codigo;
	private String autor;
	private String descripcion;
	private Double precio;
	private String tipo;
	private Double tamano;
	private Long id_categoria;
	private byte[] data;
	private Boolean activo;

	public Contenido() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getId() {
		return id;
	}
	
	
	public Double getPeso() {
		return peso;
	}

	@Override
	public void setId(Long id) {
		this.id = id;		
	}
	
	public void setPeso(Double peso) {
		this.peso = peso;		
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Double getTamano() {
		return tamano;
	}

	public void setTamano(Double tamano) {
		this.tamano = tamano;
	}

	public Long getId_categoria() {
		return id_categoria;
	}

	public void setId_categoria(Long id_categoria) {
		this.id_categoria = id_categoria;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
   
}
