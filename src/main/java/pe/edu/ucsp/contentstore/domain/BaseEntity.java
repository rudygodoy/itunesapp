package pe.edu.ucsp.contentstore.domain;

public interface BaseEntity<PK extends Number> {

	PK getId();
	void setId(PK id);
	
}
