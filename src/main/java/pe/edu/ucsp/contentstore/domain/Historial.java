package pe.edu.ucsp.contentstore.domain;

import java.util.Date;


public class Historial implements BaseEntity<Long>{

	public String TABLE_NAME = "historial";

	private Long id;
	private Date fecha;
	
	public Historial() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	

}
