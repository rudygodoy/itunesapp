package pe.edu.ucsp.contentstore.domain;

import java.util.Date;

public class Orden implements BaseEntity<Long> {

	public static final String TABLE_NAME = "ordenes";
	private Long id;
	private String codigo;
	private Date fecha;
	private Usuario usuario;
	private Contenido contenido;
	
	public Orden() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Contenido getContenido() {
		return contenido;
	}

	public void setContenido(Contenido contenido) {
		this.contenido = contenido;
	}

}
