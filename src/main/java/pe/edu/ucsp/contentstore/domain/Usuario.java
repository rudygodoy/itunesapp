package pe.edu.ucsp.contentstore.domain;

import org.hibernate.validator.constraints.Email;

public class Usuario implements BaseEntity<Long> {

	public static String TABLE_NAME = "usuarios";
	
	private Long id;
	private String codigo;
	private String tipo;
	private String nombre;
	private String apellido;
	@Email
	private String email;
	private String clave;
	private Double credito;
	private Boolean activo;

	
	public Usuario(){
	}

	
	@Override
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = Long.valueOf(id);		
	}
	
	public String getCodigo(){	
		return codigo;
	}
	
	public void setCodigo(String codigo){
		this.codigo = codigo;
	}
	
	public String getTipo(){
		return tipo;
	}
	
	public void setTipo(String tipo){
		this.tipo = tipo;
	}
	
	public String getNombre(){
		return nombre;
	}
	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public String getApellido(){
		return apellido;
	}
	
	public void setApellido(String apellido){
		this.apellido = apellido;
	}
	
	public String getEmail(){
		return email;
	}
	
	public void setEmail(String email){
		this.email = email;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}


	public Double getCredito() {
		return credito;
	}


	public void setCredito(Double credito) {
		this.credito = credito;
	}


	public Boolean getActivo() {
		return activo;
	}


	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
}
