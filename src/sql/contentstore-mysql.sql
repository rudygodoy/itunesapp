-- --------------------------------------------------------
-- Host:                         127.0.0.3
-- Versión del servidor:         5.5.27 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para db_descargas
DROP DATABASE IF EXISTS `db_descargas`;
CREATE DATABASE IF NOT EXISTS `db_descargas` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_descargas`;


-- Volcando estructura para tabla db_descargas.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `codigo` varchar(50) NOT NULL,
  `id_categoria` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `FK_categoria_categoria` (`id_categoria`),
  CONSTRAINT `FK_categoria_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_descargas.categoria: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;


-- Volcando estructura para tabla db_descargas.contenido
CREATE TABLE IF NOT EXISTS `contenido` (
  `codigo` varchar(50) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `precio` double unsigned NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `tamanio` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_descargas.contenido: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `contenido` DISABLE KEYS */;
/*!40000 ALTER TABLE `contenido` ENABLE KEYS */;


-- Volcando estructura para tabla db_descargas.historial
CREATE TABLE IF NOT EXISTS `historial` (
  `id_usuario` varchar(50) NOT NULL,
  `id_contenido` varchar(50) NOT NULL,
  `fecha` datetime NOT NULL,
  KEY `FK_historial_usuarios` (`id_usuario`),
  KEY `FK_historial_contenido` (`id_contenido`),
  CONSTRAINT `FK_historial_usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`codigo`),
  CONSTRAINT `FK_historial_contenido` FOREIGN KEY (`id_contenido`) REFERENCES `contenido` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_descargas.historial: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `historial` DISABLE KEYS */;
/*!40000 ALTER TABLE `historial` ENABLE KEYS */;


-- Volcando estructura para tabla db_descargas.orden
CREATE TABLE IF NOT EXISTS `orden` (
  `id_usuario` varchar(50) NOT NULL,
  `id_contenido` varchar(50) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `FK_orden_usuarios` (`id_usuario`),
  KEY `FK_orden_contenido` (`id_contenido`),
  CONSTRAINT `FK_orden_usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`codigo`),
  CONSTRAINT `FK_orden_contenido` FOREIGN KEY (`id_contenido`) REFERENCES `contenido` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_descargas.orden: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `orden` DISABLE KEYS */;
/*!40000 ALTER TABLE `orden` ENABLE KEYS */;


-- Volcando estructura para tabla db_descargas.promocion
CREATE TABLE IF NOT EXISTS `promocion` (
  `id_contenido` varchar(50) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `descuento` int(11) unsigned NOT NULL,
  KEY `FK_promocion_contenido` (`id_contenido`),
  CONSTRAINT `FK_promocion_contenido` FOREIGN KEY (`id_contenido`) REFERENCES `contenido` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_descargas.promocion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `promocion` DISABLE KEYS */;
/*!40000 ALTER TABLE `promocion` ENABLE KEYS */;


-- Volcando estructura para tabla db_descargas.ranking
CREATE TABLE IF NOT EXISTS `ranking` (
  `id_contenido` varchar(50) NOT NULL,
  `id_usuario` varchar(50) NOT NULL,
  `valor` int(11) unsigned NOT NULL,
  KEY `FK_ranking_contenido` (`id_contenido`),
  KEY `FK_ranking_usuarios` (`id_usuario`),
  CONSTRAINT `FK_ranking_contenido` FOREIGN KEY (`id_contenido`) REFERENCES `contenido` (`codigo`),
  CONSTRAINT `FK_ranking_usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_descargas.ranking: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ranking` DISABLE KEYS */;
/*!40000 ALTER TABLE `ranking` ENABLE KEYS */;


-- Volcando estructura para tabla db_descargas.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `codigo` varchar(50) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `login` varchar(100) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `credito` double unsigned NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_descargas.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
