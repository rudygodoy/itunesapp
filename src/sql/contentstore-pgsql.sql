--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.3
-- Dumped by pg_dump version 9.1.3
-- Started on 2013-11-09 12:24:10 PET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2187 (class 1262 OID 528561)
-- Name: contentstore; Type: DATABASE; Schema: -; Owner: htudev
--

CREATE DATABASE contentstore WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE contentstore OWNER TO contentstore;

\connect contentstore

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 175 (class 3079 OID 11907)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2190 (class 0 OID 0)
-- Dependencies: 175
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 168 (class 1259 OID 528583)
-- Dependencies: 5
-- Name: categorias_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categorias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categorias_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 169 (class 1259 OID 528585)
-- Dependencies: 2153 5
-- Name: categorias; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categorias (
    id integer DEFAULT nextval('categorias_id_seq'::regclass) NOT NULL,
    codigo character(32),
    nombre character(100),
    id_categoria integer
);


ALTER TABLE public.categorias OWNER TO postgres;

--
-- TOC entry 163 (class 1259 OID 528573)
-- Dependencies: 5
-- Name: contenidos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contenidos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contenidos_id_seq OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 528597)
-- Dependencies: 2154 5
-- Name: contenidos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE contenidos (
    id integer DEFAULT nextval('contenidos_id_seq'::regclass) NOT NULL,
    codigo character(32),
    autor character(100),
    descripcion text,
    precio double precision,
    tipo character(32),
    peso double precision,
    id_categoria integer
);


ALTER TABLE public.contenidos OWNER TO postgres;

--
-- TOC entry 164 (class 1259 OID 528575)
-- Dependencies: 5
-- Name: historial_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE historial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historial_id_seq OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 528606)
-- Dependencies: 2155 5
-- Name: historial; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE historial (
    id integer DEFAULT nextval('historial_id_seq'::regclass) NOT NULL,
    id_usuario integer NOT NULL,
    id_contenido integer NOT NULL,
    fecha timestamp without time zone NOT NULL
);


ALTER TABLE public.historial OWNER TO postgres;

--
-- TOC entry 165 (class 1259 OID 528577)
-- Dependencies: 5
-- Name: ordenes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ordenes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ordenes_id_seq OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 528622)
-- Dependencies: 2156 5
-- Name: ordenes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ordenes (
    id integer DEFAULT nextval('ordenes_id_seq'::regclass) NOT NULL,
    id_usuario integer NOT NULL,
    id_contenido integer NOT NULL,
    codigo character(32),
    fecha time without time zone
);


ALTER TABLE public.ordenes OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 528579)
-- Dependencies: 5
-- Name: promociones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE promociones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.promociones_id_seq OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 528644)
-- Dependencies: 2157 5
-- Name: promociones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE promociones (
    id integer DEFAULT nextval('promociones_id_seq'::regclass) NOT NULL,
    id_contenido integer NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date NOT NULL,
    descuento double precision
);


ALTER TABLE public.promociones OWNER TO postgres;

--
-- TOC entry 167 (class 1259 OID 528581)
-- Dependencies: 5
-- Name: ranking_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ranking_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ranking_id_seq OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 528659)
-- Dependencies: 2158 2159 5
-- Name: ranking; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ranking (
    id integer DEFAULT nextval('ranking_id_seq'::regclass) NOT NULL,
    id_contenido integer NOT NULL,
    id_usuario integer NOT NULL,
    valor integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ranking OWNER TO postgres;

--
-- TOC entry 162 (class 1259 OID 528565)
-- Dependencies: 5
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO postgres;

--
-- TOC entry 161 (class 1259 OID 528562)
-- Dependencies: 2152 5
-- Name: usuarios; Type: TABLE; Schema: public; Owner: htudev; Tablespace: 
--

CREATE TABLE usuarios (
    id integer DEFAULT nextval('usuarios_id_seq'::regclass) NOT NULL,
    nombre character varying,
    apellido character varying,
    codigo character(16),
    tipo character(32),
    email character(64),
    clave character(256),
    credito double precision
);


ALTER TABLE public.usuarios OWNER TO htudev;

--
-- TOC entry 2163 (class 2606 OID 528590)
-- Dependencies: 169 169
-- Name: categorias_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categorias
    ADD CONSTRAINT categorias_pk PRIMARY KEY (id);


--
-- TOC entry 2166 (class 2606 OID 528605)
-- Dependencies: 170 170
-- Name: contenidos_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY contenidos
    ADD CONSTRAINT contenidos_pk PRIMARY KEY (id);


--
-- TOC entry 2169 (class 2606 OID 528611)
-- Dependencies: 171 171
-- Name: historial_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY historial
    ADD CONSTRAINT historial_pk PRIMARY KEY (id);


--
-- TOC entry 2171 (class 2606 OID 528627)
-- Dependencies: 172 172
-- Name: ordenes_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ordenes
    ADD CONSTRAINT ordenes_pk PRIMARY KEY (id);


--
-- TOC entry 2173 (class 2606 OID 528649)
-- Dependencies: 173 173
-- Name: promociones_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY promociones
    ADD CONSTRAINT promociones_pk PRIMARY KEY (id);


--
-- TOC entry 2175 (class 2606 OID 528664)
-- Dependencies: 174 174
-- Name: ranking_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ranking
    ADD CONSTRAINT ranking_pk PRIMARY KEY (id);


--
-- TOC entry 2161 (class 2606 OID 528569)
-- Dependencies: 161 161
-- Name: usuarios_pk; Type: CONSTRAINT; Schema: public; Owner: htudev; Tablespace: 
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT usuarios_pk PRIMARY KEY (id);


--
-- TOC entry 2167 (class 1259 OID 528643)
-- Dependencies: 170
-- Name: fki_contenido_categoria_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_contenido_categoria_fk ON contenidos USING btree (id_categoria);


--
-- TOC entry 2164 (class 1259 OID 528596)
-- Dependencies: 169
-- Name: fki_subcategorias_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_subcategorias_fk ON categorias USING btree (id_categoria);


--
-- TOC entry 2177 (class 2606 OID 528638)
-- Dependencies: 2162 169 170
-- Name: contenido_categoria_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contenidos
    ADD CONSTRAINT contenido_categoria_fk FOREIGN KEY (id_categoria) REFERENCES categorias(id);


--
-- TOC entry 2179 (class 2606 OID 528617)
-- Dependencies: 171 2165 170
-- Name: historial_contenido_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY historial
    ADD CONSTRAINT historial_contenido_fk FOREIGN KEY (id_contenido) REFERENCES contenidos(id);


--
-- TOC entry 2178 (class 2606 OID 528612)
-- Dependencies: 171 161 2160
-- Name: historial_usuario_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY historial
    ADD CONSTRAINT historial_usuario_fk FOREIGN KEY (id_usuario) REFERENCES usuarios(id);


--
-- TOC entry 2181 (class 2606 OID 528633)
-- Dependencies: 170 172 2165
-- Name: ordenes_contenido_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ordenes
    ADD CONSTRAINT ordenes_contenido_fk FOREIGN KEY (id_contenido) REFERENCES contenidos(id);


--
-- TOC entry 2180 (class 2606 OID 528628)
-- Dependencies: 2160 161 172
-- Name: ordenes_usuario_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ordenes
    ADD CONSTRAINT ordenes_usuario_fk FOREIGN KEY (id_usuario) REFERENCES usuarios(id);


--
-- TOC entry 2182 (class 2606 OID 528650)
-- Dependencies: 170 173 2165
-- Name: promociones_contenido_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY promociones
    ADD CONSTRAINT promociones_contenido_fk FOREIGN KEY (id_contenido) REFERENCES contenidos(id);


--
-- TOC entry 2183 (class 2606 OID 528676)
-- Dependencies: 170 2165 174
-- Name: ranking_contenido_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ranking
    ADD CONSTRAINT ranking_contenido_fk FOREIGN KEY (id_contenido) REFERENCES contenidos(id);


--
-- TOC entry 2184 (class 2606 OID 528681)
-- Dependencies: 161 174 2160
-- Name: ranking_usuario_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ranking
    ADD CONSTRAINT ranking_usuario_fk FOREIGN KEY (id_usuario) REFERENCES usuarios(id);


--
-- TOC entry 2176 (class 2606 OID 528591)
-- Dependencies: 2162 169 169
-- Name: subcategorias_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categorias
    ADD CONSTRAINT subcategorias_fk FOREIGN KEY (id_categoria) REFERENCES categorias(id);


--
-- TOC entry 2189 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-11-09 12:24:10 PET

--
-- PostgreSQL database dump complete
--

